package by.training.buyers.view;

import by.training.buyers.Constants;

import javax.swing.*;

public class CashBoxLabel extends JLabel {

    public CashBoxLabel() {
        setIcon(new ImageIcon(Constants.PATH_IMAGE_CASHBOX));
        setVerticalTextPosition(JLabel.BOTTOM);
        setHorizontalTextPosition(JLabel.CENTER);
        setText(Constants.LABEL_CASH_BOX);
        setBorder(BorderFactory.createRaisedBevelBorder());
    }
}
