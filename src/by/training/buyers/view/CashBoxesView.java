package by.training.buyers.view;

import by.training.buyers.CashBoxesController;
import by.training.buyers.beans.CashBox;

import javax.swing.*;
import java.awt.*;

public class CashBoxesView extends JPanel {

    private CashBoxesController cashBoxesController;

    public CashBoxesView(CashBoxesController cashBoxesController) {
        super();
        this.cashBoxesController = cashBoxesController;
        initialize();
    }

    private void initialize() {
        BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(boxLayout);
        for (CashBox cashBox : cashBoxesController.getCashBoxes()) {
            this.add(cashBox.getCashBoxView());
        }
    }
}
