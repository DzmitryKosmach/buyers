package by.training.buyers.view;

import by.training.buyers.CashBoxesController;
import by.training.buyers.Constants;
import by.training.buyers.utils.NumberGenerator;
import by.training.buyers.beans.ShopFloor;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private JPanel jContentPane = null;
    private CashBoxesView cashBoxesView = null;
    private ShopFloorView shopFloorView = null;
    private ShopFloor shopFloor;
    private CashBoxesController cashBoxesController;

    public MainFrame() {
        super();
        shopFloorView = new ShopFloorView();
        shopFloor = new ShopFloor(shopFloorView);
        cashBoxesController = new CashBoxesController(NumberGenerator.getRandomNumber(Constants.MIN_NUMBER_CASHBOXES, Constants.MAX_NUMBER_CASHBOXES), shopFloor);
        cashBoxesView = new CashBoxesView(cashBoxesController);
        initialize();
    }

    private void initialize() {
        this.setSize(1280, 720);
        this.setName("Buyers");
        this.setLocationRelativeTo(null);
        this.setContentPane(getJContentPane());
        this.setTitle("Buyers");
        this.setIconImage(new ImageIcon(Constants.PATH_IMAGE_BUYER).getImage());
    }

    private JPanel getJContentPane() {
        if (jContentPane == null) {

            jContentPane = new JPanel();
            jContentPane.setLayout(new GridLayout(1, 2));
            jContentPane.add(cashBoxesView);
            jContentPane.add(shopFloorView);
        }
        return jContentPane;
    }

    public CashBoxesView getCashBoxesView() {
        return cashBoxesView;
    }

    public ShopFloorView getShopFloorView() {
        return shopFloorView;
    }

    public ShopFloor getShopFloor() {
        return shopFloor;
    }

    public CashBoxesController getCashBoxesController() {
        return cashBoxesController;
    }
}
