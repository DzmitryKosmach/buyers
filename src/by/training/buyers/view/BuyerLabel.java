package by.training.buyers.view;

import by.training.buyers.Constants;

import javax.swing.*;
import java.awt.*;

public class BuyerLabel extends JLabel {

    public BuyerLabel() {
        super();
        setBorder(BorderFactory.createRaisedBevelBorder());
        setVerticalTextPosition(JLabel.BOTTOM);
        setHorizontalTextPosition(JLabel.CENTER);
        setText(Constants.LABEL_NUMBER_PURCHASES_0);
        setIcon(new ImageIcon(Constants.PATH_IMAGE_BUYER));
    }

    public BuyerLabel(boolean insolent) {
        this();
        if (insolent) {
            setBackground(Color.RED);
        }
    }

    public void setPurchases(int purchases) {
        this.setText(String.valueOf(purchases));
    }


}
