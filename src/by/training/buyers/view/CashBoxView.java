package by.training.buyers.view;

import by.training.buyers.Constants;
import by.training.buyers.beans.CashBox;

import javax.swing.*;
import java.awt.*;

public class CashBoxView extends JPanel {
    private JPanel cashBoxPanel;
    private JLabel cashBoxLabel;

    public CashBoxView(){
        super();
        initialize();
    }

    private void initialize() {
        cashBoxPanel = new JPanel();
        cashBoxLabel = new CashBoxLabel();
        cashBoxPanel.add(cashBoxLabel);
        add(cashBoxPanel);
        this.setBorder(BorderFactory.createRaisedBevelBorder());
    }

    public void setCashBoxLabelText(String labelText) {
        cashBoxLabel.setText(labelText);
    }

    public void addBuyerLabel(JLabel label){
        cashBoxPanel.add(label);
    }

    public void removeBuyerLabel(JLabel label){
        cashBoxPanel.remove(label);
    }



}
