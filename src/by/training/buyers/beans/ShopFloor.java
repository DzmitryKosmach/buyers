package by.training.buyers.beans;

import by.training.buyers.tasks.BuyTask;
import by.training.buyers.view.ShopFloorView;

import java.util.HashSet;
import java.util.Set;

public class ShopFloor {
    private Set<BuyTask> buyTasks;
    private ShopFloorView shopFloorView;

    public ShopFloor(ShopFloorView shopFloorView) {
        super();
        buyTasks = new HashSet<>();
        this.shopFloorView = shopFloorView;
    }

    public void addBuyTask(BuyTask buyTask) {
        buyTasks.add(buyTask);
        shopFloorView.add(buyTask.getBuyerLabel());
        shopFloorView.repaint();
    }

    public BuyTask removeBuyTask(BuyTask buyTask) {
        if (buyTasks.remove(buyTask)) {
            shopFloorView.remove(buyTask.getBuyerLabel());
            shopFloorView.repaint();
            return buyTask;
        }
        return null;
    }

    public void makePurchase (BuyTask buyTask){
        Buyer buyer = buyTask.getBuyer();
        buyer.setNumberPurchases(buyer.getNumberPurchases()+1);
        buyTask.getBuyerLabel().setPurchases(buyer.getNumberPurchases());
    }
}
