package by.training.buyers.beans;

import by.training.buyers.tasks.BuyTask;
import by.training.buyers.Constants;
import by.training.buyers.view.CashBoxView;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.Semaphore;

public class CashBox {
    private Deque<BuyTask> buyTasks = null;
    private CashBoxView cashBoxView;
    private boolean lunch;
    private Semaphore lunchSemaphore;
    private Semaphore startLunchSemaphore;
    private BuyTask currentBuyTask;

    public CashBox() {
        buyTasks = new LinkedList<>();
        cashBoxView = new CashBoxView();
        lunch = false;
        lunchSemaphore = new Semaphore(1);
        startLunchSemaphore = new Semaphore(0);
    }

    public Semaphore getStartLunchSemaphore() {
        return startLunchSemaphore;
    }

    public void setCurrentBuyTask(BuyTask currentBuyTask) {
        this.currentBuyTask = currentBuyTask;
    }

    public CashBoxView getCashBoxView() {
        return cashBoxView;
    }

    public void addBuyTask(BuyTask buyTask) {
        buyTasks.add(buyTask);
        cashBoxView.addBuyerLabel(buyTask.getBuyerLabel());
        cashBoxView.repaint();
    }

    public BuyTask getFirstBuyTask() {
        return buyTasks.peekFirst();
    }

    public BuyTask getInsolentBuyTask() {
        BuyTask insolentBuyTask = buyTasks.peekLast();
        if (insolentBuyTask != null) {
            if (insolentBuyTask.getBuyer().isInsolent()) {
                return null;
            }
        }
        return null;
    }

    public BuyTask removeFirstBuyTask() {
        BuyTask buyTask = buyTasks.removeFirst();
        if (buyTask != null) {
            cashBoxView.removeBuyerLabel(buyTask.getBuyerLabel());
        }
        return buyTask;
    }

    public BuyTask removeCurrentBuyTask() {
        if (currentBuyTask != null) {
            buyTasks.remove(currentBuyTask);
            cashBoxView.removeBuyerLabel(currentBuyTask.getBuyerLabel());
            cashBoxView.repaint();
        }
        return currentBuyTask;
    }


    public int getBuyersSize() {
        return buyTasks.size();
    }

    public int getNumberPurchases() {
        int numberPurchases = 0;
        for (BuyTask buyTask : buyTasks) {
            numberPurchases += buyTask.getBuyer().getNumberPurchases();
        }
        return numberPurchases;
    }

    public Set<BuyTask> removeBuyTasks() {
        Set<BuyTask> buyTasksToOut = new HashSet<>();
        while (buyTasks.size() != 0) {
            buyTasksToOut.add(removeFirstBuyTask());
        }
        return buyTasksToOut;
    }

    public void startLunch() throws InterruptedException {
        lunchSemaphore.acquire();
        lunch = true;
        cashBoxView.setCashBoxLabelText(Constants.LABEL_TEXT_LUNCH);
        cashBoxView.repaint();
    }

    public boolean isLunch() {
        return lunch;
    }

    public void endLunch() {
        lunch = false;
        lunchSemaphore.release();
        cashBoxView.setCashBoxLabelText(Constants.LABEL_CASH_BOX);
    }

    public Semaphore getLunchSemaphore() {
        return lunchSemaphore;
    }
}
