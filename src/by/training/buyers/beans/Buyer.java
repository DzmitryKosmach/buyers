package by.training.buyers.beans;

public class Buyer {
    private int numberPurchases;
    private boolean insolent;

    public Buyer() {
       super();
    }

    public Buyer(boolean insolent) {
        this.insolent = insolent;
    }

    public Buyer(int numberPurchases) {
        this.numberPurchases = numberPurchases;
    }

    public boolean isInsolent() {
        return insolent;
    }


    public void setNumberPurchases(int numberPurchases) {
        this.numberPurchases = numberPurchases;
    }

    public int getNumberPurchases() {
        return numberPurchases;
    }

    public void cancelInsolent(){
        insolent = false;
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "numberPurchases=" + numberPurchases +
                ", insolent=" + insolent +
                '}';
    }
}
