package by.training.buyers;

import by.training.buyers.beans.CashBox;
import by.training.buyers.beans.ShopFloor;
import by.training.buyers.tasks.BuyTask;
import by.training.buyers.tasks.CashBoxTask;
import by.training.buyers.tasks.LunchTask;
import by.training.buyers.utils.NumberGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CashBoxesController {
    private List<CashBox> cashBoxes;
    private ShopFloor shopFloor;
    private ExecutorService executorService;
    private ScheduledExecutorService scheduledExecutorService;

    public CashBoxesController(int numberCashBoxes, ShopFloor shopFloor) {
        cashBoxes = new ArrayList<CashBox>();
        this.shopFloor = shopFloor;
        executorService = Executors.newFixedThreadPool(numberCashBoxes);
        createCashBoxes(numberCashBoxes);
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new LunchTask(this, shopFloor), 10000, 20000, TimeUnit.MILLISECONDS);
    }

    public List<CashBox> getCashBoxes() {
        return cashBoxes;
    }

    private void createCashBoxes(int numberCashBoxes) {
        if (numberCashBoxes < Constants.MIN_NUMBER_CASHBOXES) {
            numberCashBoxes = Constants.MIN_NUMBER_CASHBOXES;
        }
        for (int i = 0; i < numberCashBoxes; i++) {
            CashBox cashBox = new CashBox();
            cashBoxes.add(cashBox);
            executorService.execute(new CashBoxTask(cashBox));
        }
    }

    public CashBox getCashBoxMinQueue() {
        CashBox cashBoxMinQueue = null;
        for (CashBox cashBox : cashBoxes) {
            if (!cashBox.isLunch()) {
                if(cashBoxMinQueue == null){
                    cashBoxMinQueue = cashBox;
                }else if (cashBox.getBuyersSize() < cashBoxMinQueue.getBuyersSize()){
                    cashBoxMinQueue = cashBox;
                }
            }
        }
        return cashBoxMinQueue;
    }

    public CashBox getCashBoxMinPurchases() {
        CashBox cashBoxMinPurchases = null;
        for (CashBox cashBox : cashBoxes) {
            if (!cashBox.isLunch()) {
                if(cashBoxMinPurchases == null){
                    cashBoxMinPurchases = cashBox;
                }else if (cashBox.getNumberPurchases() < cashBoxMinPurchases.getNumberPurchases()){
                    cashBoxMinPurchases = cashBox;
                }
            }
        }
        return cashBoxMinPurchases;
    }

    /**
     * Randomly chooses the cash box with the lowest queue or with the smallest number of purchases
     * and adds the Buyer in queue.
     */
    public synchronized void pushBuyTaskInQueue(BuyTask buyTask) {
        CashBox cashBox;
        if (NumberGenerator.isHeadsOrTails()) {
            cashBox = getCashBoxMinQueue();
        } else {
            cashBox =  getCashBoxMinPurchases();
        }
        cashBox.addBuyTask(buyTask);
    }

}
