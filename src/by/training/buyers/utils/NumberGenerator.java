package by.training.buyers.utils;

import java.util.Random;

public class NumberGenerator {

    private static final Random RANDOM = new Random();


    public static int getRandomNumber(int max) {
        return RANDOM.nextInt(max) + 1;
    }

    public static int getRandomNumber(int min, int max) {
        return RANDOM.nextInt((max - min) + 1) + min;
    }

    public static boolean isHeadsOrTails(){
        return (getRandomNumber(2)==1);
    }

    public static boolean isHeadsOrTailsDegree(int degree){
        return (getRandomNumber(degree)==1);
    }
}
