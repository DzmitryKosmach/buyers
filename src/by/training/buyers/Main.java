package by.training.buyers;

import by.training.buyers.tasks.BuyTask;
import by.training.buyers.view.MainFrame;

import javax.swing.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame frame = new MainFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(Constants.NUMBER_BUYERS);
                for (int i = 0; i <= executor.getCorePoolSize(); i++) {
                    executor.schedule(new BuyTask(frame), Constants.NEW_BUYER_TIME * i, TimeUnit.MILLISECONDS);
                }
            }
        });
    }
}
