package by.training.buyers.tasks;

import by.training.buyers.CashBoxesController;
import by.training.buyers.Constants;
import by.training.buyers.utils.NumberGenerator;
import by.training.buyers.beans.Buyer;
import by.training.buyers.beans.ShopFloor;
import by.training.buyers.view.BuyerLabel;
import by.training.buyers.view.MainFrame;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class BuyTask implements Runnable {
    private Buyer buyer;
    private BuyerLabel buyerLabel;
    private ShopFloor shopFloor;
    private CashBoxesController cashBoxesController;
    private CashBoxTask cashBoxTask;
    private CountDownLatch startServingLatch;
    private CountDownLatch exitLatch;
    private BuyTask concurrentBuyTask;
    private boolean skipping;


    public BuyTask(MainFrame mainFrame) {
        boolean insolent = randomInsolent();
        buyer = new Buyer(insolent);
        buyerLabel = new BuyerLabel(insolent);
        cashBoxesController = mainFrame.getCashBoxesController();
        shopFloor = mainFrame.getShopFloor();
        startServingLatch = new CountDownLatch(1);
        exitLatch = new CountDownLatch(1);
        skipping = false;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public BuyerLabel getBuyerLabel() {
        return buyerLabel;
    }

    public void setCashBoxTask(CashBoxTask cashBoxTask) {
        this.cashBoxTask = cashBoxTask;
    }

    public void setConcurrentBuyTask(BuyTask concurrentBuyTask) {
        this.concurrentBuyTask = concurrentBuyTask;
    }

    public BuyTask getConcurrentBuyTask() {
        return concurrentBuyTask;
    }

    private boolean randomInsolent() {
        return NumberGenerator.isHeadsOrTailsDegree(Constants.INSOLENT_DEGREE);
    }

    public void getQueue() {
        shopFloor.removeBuyTask(this);
        cashBoxesController.pushBuyTaskInQueue(this);
    }


    public void layOutPurchase() {
        buyer.setNumberPurchases(buyer.getNumberPurchases() - 1);
        buyerLabel.setPurchases(buyer.getNumberPurchases());
    }

    public void serveBuyer() {
        startServingLatch.countDown();
    }

    public void breakServing(){
        startServingLatch = new CountDownLatch(1);
        skipping = true;
    }

    public void getOutBuyer() {
        exitLatch.countDown();
    }


    @Override
    public void run() {

        try {
            // The buyer came in the shop
            shopFloor.addBuyTask(this);

            // The buyer is making purchases
            for (int i = 0; i < NumberGenerator.getRandomNumber(Constants.MAX_PURCHASES); i++) {
                shopFloor.makePurchase(this);
                Thread.sleep(NumberGenerator.getRandomNumber(Constants.MIN_PURCHASES_TIME, Constants.MAX_PURCHASES_TIME));
            }

            // The buyer is getting queue.
            getQueue();

            // The buyer is waiting for serving
            startServingLatch.await();

            // The buyer is waiting access to cash box
            cashBoxTask.getAccessCashBox().acquire();

            // Skipping for second thread which did't capture the cash box
            if(!skipping) {
                BuyTask concurrentBuyTask = getConcurrentBuyTask();
                if (concurrentBuyTask != null) {
                    concurrentBuyTask.breakServing();
                    concurrentBuyTask.getBuyer().cancelInsolent();
                }
                //cashBoxTask.getCashBox().removeBuyTaskFromQueue(this);
                cashBoxTask.getCashBox().setCurrentBuyTask(this);
            }

            // The buyer is waiting for serving
            startServingLatch.await();

            // The buyer lays out purchases
            Thread.sleep(Constants.ANIMATE_DELAY);
            int numberPurchases = buyer.getNumberPurchases();
            while (numberPurchases > 0) {
                layOutPurchase();
                numberPurchases = buyer.getNumberPurchases();
                Thread.sleep(Constants.ANIMATE_DELAY);
            }

            cashBoxTask.getReadinessBuyer().release();

            // The buyer has served
            exitLatch.await();

            cashBoxTask.getReadinessBuyer().release();

            cashBoxTask.getAccessCashBox().release();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

}
