package by.training.buyers.tasks;

import by.training.buyers.CashBoxesController;
import by.training.buyers.Constants;
import by.training.buyers.utils.NumberGenerator;
import by.training.buyers.beans.CashBox;
import by.training.buyers.beans.ShopFloor;

import java.util.List;
import java.util.Set;

public class LunchTask implements Runnable {
    CashBoxesController cashBoxesController;
    ShopFloor shopFloor;

    public LunchTask(CashBoxesController cashBoxesController, ShopFloor shopFloor) {
        this.cashBoxesController = cashBoxesController;
        this.shopFloor = shopFloor;
    }

    @Override
    public void run() {
        List<CashBox> cashBoxes = cashBoxesController.getCashBoxes();
        int IndexCashBox = NumberGenerator.getRandomNumber(cashBoxes.size()) - 1;
        CashBox cashBox = cashBoxes.get(IndexCashBox);

        try {
            cashBox.startLunch();
            cashBox.getStartLunchSemaphore().acquire();
            Set<BuyTask> buyTasks = cashBox.removeBuyTasks();
            for (BuyTask buyTask : buyTasks) {
                shopFloor.addBuyTask(buyTask);
                buyTask.getQueue();
            }
            Thread.sleep(Constants.LUNCH_TIME);
            cashBox.endLunch();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cashBox.endLunch();
    }
}
