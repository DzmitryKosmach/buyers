package by.training.buyers.tasks;

import by.training.buyers.Constants;
import by.training.buyers.beans.Buyer;
import by.training.buyers.beans.CashBox;
import by.training.buyers.tasks.BuyTask;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class CashBoxTask implements Runnable {

    private CashBox cashBox;

    private Semaphore accessCashBox;
    private Semaphore readinessBuyer;

    public CashBoxTask(CashBox cashBox) {
        this.cashBox = cashBox;
        accessCashBox = new Semaphore(0, false);
        readinessBuyer = new Semaphore(0);
    }

    public Semaphore getAccessCashBox() {
        return accessCashBox;
    }

    public Semaphore getReadinessBuyer() {
        return readinessBuyer;
    }

    public CashBox getCashBox() {
        return cashBox;
    }

    @Override
    public void run() {

        try {
            Semaphore lunchSemaphore = cashBox.getLunchSemaphore();

            while (true) {
                BuyTask firstBuyTask = cashBox.getFirstBuyTask();
                BuyTask insolentBuyTask = null;
                if (cashBox.getBuyersSize() > 1) {
                    insolentBuyTask = cashBox.getInsolentBuyTask();
                }

                if (firstBuyTask != null) {
                    if (firstBuyTask != null) {
                        firstBuyTask.setCashBoxTask(this);
                        firstBuyTask.setConcurrentBuyTask(insolentBuyTask);
                        firstBuyTask.serveBuyer();
                    }
                    if (insolentBuyTask != null) {
                        insolentBuyTask.setCashBoxTask(this);
                        insolentBuyTask.setConcurrentBuyTask(firstBuyTask);
                        insolentBuyTask.serveBuyer();
                    }

                    accessCashBox.release();

                    readinessBuyer.acquire();

                    // The buyer is paying off and going out
                    cashBox.removeCurrentBuyTask().getOutBuyer();

                    readinessBuyer.acquire();
                }

                if (cashBox.isLunch()) {
                    cashBox.getStartLunchSemaphore().release();

                }

                // If it is a lunch - waiting for end one
                lunchSemaphore.acquire();
                lunchSemaphore.release();

                Thread.sleep(Constants.DELAY_100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
    }
}

