package by.training.buyers;

public class Constants {

    public static final int MIN_NUMBER_CASHBOXES = 2;
    public static final int MAX_NUMBER_CASHBOXES = 6;
    public static final int NUMBER_BUYERS = 100;
    public static final int UNIT_TIME = 1000;
    public static final int NEW_BUYER_TIME = 1 * UNIT_TIME;
    public static final int MAX_PURCHASES = 10;
    public static final int MIN_PURCHASES_TIME = 1 * UNIT_TIME;
    public static final int MAX_PURCHASES_TIME = 2 * UNIT_TIME;
    public static final String PATH_IMAGES = "resources/img/";
    public static final String PATH_IMAGE_CASHBOX = PATH_IMAGES + "cashbox.png";
    public static final String PATH_IMAGE_BUYER = PATH_IMAGES + "buyer.png";
    public static final String LABEL_CASH_BOX = "Cash Box";
    public static final String LABEL_TEXT_LUNCH = "Lunch!";
    public static final String LABEL_NUMBER_PURCHASES_0 = "0";
    public static final int INSOLENT_DEGREE = 3;
    public static final int ANIMATE_DELAY = 1 * UNIT_TIME;
    public static final int DELAY_100 = UNIT_TIME / 10;
    public static final int DELAY_300 = UNIT_TIME/3;
    public static final int LUNCH_TIME = 10 * UNIT_TIME;
    public static final String NAME_APPLICATION = "Buyers";

}
